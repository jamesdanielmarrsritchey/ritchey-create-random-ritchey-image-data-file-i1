<?php
#Name:Create Random Ritchey Image Data File v1
#Description:Create a random image file in Ritchey Image Data format. Returns image path as string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data will contain a field called "Colour", and it will have an RGB colour code as the value. If no width, or height are specified they'll be random (max 50,0000).
#Arguments:'destination' (required) is the path to save the image file to. 'width' (optional) is a number specifying how many pixels wide the image should be. 'height' (optional) is a specifying how many pixels tall the image should be. 'limit' (optional) indicates whether to limit future colour code minimum values to the highest used previous value. It will reset to 0 if 255 is reached. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):destination:required:path,width:number:optional,height:number:optional,limit:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('create_random_ritchey_image_data_file_v1') === FALSE){
function create_random_ritchey_image_data_file_v1($destination, $width = NULL, $height = NULL, $limit = NULL, $display_errors = NULL){
	$errors = array();
	if (@is_dir(@dirname($destination)) === FALSE){
		$errors[] = "destination";
	}
	if ($width === NULL){
		$width = rand(1, 50000);
	}
	if (@is_int($width) === FALSE){
		$errors[] = "width";
	}
	if ($height === NULL){
		$height = rand(1, 50000);
	}
	if (@is_int($height) === FALSE){
		$errors[] = "height";
	}
	if ($limit === NULL){
		$limit = FALSE;
	} else if ($limit === TRUE){
		#Do Nothing
	} else if ($limit === FALSE){
		#Do Nothing
	} else {
		$errors[] = "limit";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Using a loop, create the number of required pixels with random colours, and add a line break every nth pixel to create the expected width or height]
	if (@empty($errors) === TRUE){
		###Determine total_pixels
		$total_pixels = $width * $height;
		###Create image
		$data = '';
		$pixels_in_current_row = 1;
		$min_colour_third = 0;
		for ($i = 1; $i <= $total_pixels; $i++){
			$data = $data . "[Colour:";
			if ($limit === TRUE){
				if (@isset($colour_third) === TRUE){
					if ($colour_third > $min_colour_third){
						$min_colour_third = $colour_third;
					} else if ($min_colour_third === 255){
						$min_colour_third = 0;
					}
				}
			}
			$colour_third = @rand($min_colour_third, 255);
			$data = $data . $colour_third;
			$data = $data . ',';
			$colour_third = @rand($min_colour_third, 255);
			$data = $data . $colour_third;
			$data = $data . ',';
			$colour_third = @rand($min_colour_third, 255);
			$data = $data . $colour_third;
			$data = $data . '.]';
			if ($pixels_in_current_row === $width){
				$data = $data . "\n";
				$pixels_in_current_row = 1;
			} else {
				$pixels_in_current_row++;
			}
			@file_put_contents($destination, $data, FILE_APPEND);
			$data = '';
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('create_random_ritchey_image_data_file_v1_format_error') === FALSE){
				function create_random_ritchey_image_data_file_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("create_random_ritchey_image_data_file_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $data;
	} else {
		return FALSE;
	}
}
}
?>